# Evolution vs Intelligent Design

It's been stated that life on Earth is intelligently designed because of how complex it is. To demonstrate this, I've created two functions to print the words "Hello World"; one that was intelligently designed and the other that operates like evolved systems. This is not intended to "disprove god" or whatever. It's just a demonstration of good vs natural design.

Special thanks to Viced Rhino for the idea
